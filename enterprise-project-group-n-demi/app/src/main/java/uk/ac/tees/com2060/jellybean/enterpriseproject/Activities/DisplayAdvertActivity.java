package uk.ac.tees.com2060.jellybean.enterpriseproject.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

public class DisplayAdvertActivity extends AppCompatActivity {

    private FirebaseStorage storage;
    private TextView mTitle, mDesc;
    private ImageView mImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_advert);

        Intent i = getIntent();
        if(i.getStringExtra("adId") != null) {
            storage = FirebaseStorage.getInstance();
            StorageReference gsReference = storage.getReferenceFromUrl("gs://enterprise-project-52aff.appspot.com");

            Log.d("AD_ID", i.getStringExtra("adId"));
            StorageReference imageRef = gsReference.child("adImages/"+i.getStringExtra("adId")+".jpg");
            mImage = findViewById(R.id.image);

            Glide.with(this)
                    .using(new FirebaseImageLoader())
                    .load(imageRef)
                    .into(mImage);

            mTitle = findViewById(R.id.title);
            mDesc = findViewById(R.id.description);

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("adverts/"+i.getStringExtra("adId"));

            ValueEventListener vel = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // Get Post object and use the values to update the UI
                    mTitle.setText(dataSnapshot.child("Title").getValue().toString());
                    mDesc.setText(dataSnapshot.child("Description").getValue().toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    // ...
                }
            };


        }
    }
}
