package uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.Calendar;

import uk.ac.tees.com2060.jellybean.enterpriseproject.Activities.CreateAdvertActivity;
import uk.ac.tees.com2060.jellybean.enterpriseproject.Activities.MainActivity;
import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

public class CreateAdvertFragment extends Fragment {

    private View createAdvertFragment;
    private ImageView image;
    private EditText title, description, location, destination;
    private TextView collectionDate, deliveryDate;
    private DatePickerDialog.OnDateSetListener collectionDateSetListener, deliveryDateSetListener;
    private Button cancel, submit;
    static final int PICK_IMAGE_REQUEST = 1;
    private static final int RESULT_OK = -1;
    private Uri filepath;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        createAdvertFragment = inflater.inflate(R.layout.create_advert_fragment, container, false);

        image = (ImageView) createAdvertFragment.findViewById(R.id.image);
        title = (EditText) createAdvertFragment.findViewById(R.id.advertTitle);
        description = (EditText) createAdvertFragment.findViewById(R.id.description);
        location =  (EditText) createAdvertFragment.findViewById(R.id.location);
        destination = (EditText) createAdvertFragment.findViewById(R.id.destination);
        collectionDate = (TextView) createAdvertFragment.findViewById(R.id.date1);
        deliveryDate = (TextView) createAdvertFragment.findViewById(R.id.date2);
        cancel = (Button) createAdvertFragment.findViewById(R.id.cancel);
        submit = (Button) createAdvertFragment.findViewById(R.id.submit);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });

        collectionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getActivity(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        collectionDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        collectionDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = "";
                if(day<10) date += "0"+day+"/"; else date += day+"/";
                if(month<10) date += "0"+month+"/"+year; else date += month+"/"+year;
                collectionDate.setText(date);
            }
        };

        deliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getActivity(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        deliveryDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        deliveryDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = "";
                if(day<10) date += "0"+day+"/"; else date += day+"/";
                if(month<10) date += "0"+month+"/"+year; else date += month+"/"+year;
                deliveryDate.setText(date);
            }
        };

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                String tit = title.getText().toString();
                String desc = description.getText().toString();
                String loc1 = location.getText().toString();
                String loc2 = destination.getText().toString();
                String date1 = collectionDate.getText().toString();
                String date2 = deliveryDate.getText().toString();

                writeDatabase(userId, tit, desc, loc1, loc2, date1, date2, filepath);

                FragmentManager fragManager = getFragmentManager();
                fragManager.beginTransaction().replace(R.id.flContent, new HomeFragment()).commit();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragManager = getFragmentManager();
                fragManager.beginTransaction().replace(R.id.flContent, new HomeFragment()).commit();
            }
        });

        return createAdvertFragment;
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filepath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filepath);
                image.setImageBitmap(bitmap);


            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public StorageReference uploadImage(Uri image, final String adId) {
        StorageReference imageRef = FirebaseStorage.getInstance().getReference().child("adImages/"+adId+".jpg");

        imageRef.putFile(image).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
        return imageRef;
    }

    public void writeDatabase(String userId, String title, String desc, String loc1, String loc2, String date1, String date2, Uri image) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference dbRoot = db.getReference("adverts");
        String adRef = dbRoot.push().getKey().toString();
        DatabaseReference dbUserId = db.getReference("adverts/"+adRef+"/userId");
        DatabaseReference dbTitle = db.getReference("adverts/"+adRef+"/title");
        DatabaseReference dbDesc = db.getReference("adverts/"+adRef+"/description");
        DatabaseReference dbLocation = db.getReference("adverts/"+adRef+"/location");
        DatabaseReference dbDestination = db.getReference("adverts/"+adRef+"/destination");
        DatabaseReference dbDate1 = db.getReference("adverts/"+adRef+"/collectionDate");
        DatabaseReference dbDate2 = db.getReference("adverts/"+adRef+"/deliveryDate");
        DatabaseReference dbImage = db.getReference("adverts/"+adRef+"/image");

//        DatabaseReference temp = db.getReference("adverts/"+adRef+"/test");
//        temp.setValue("test");
        dbUserId.setValue(userId);
        dbTitle.setValue(title);
        dbDesc.setValue(desc);
        dbLocation.setValue(loc1);
        dbDestination.setValue(loc2);
        dbDate1.setValue(date1);
        dbDate2.setValue(date2);
        if(image != null) {
            String imageRef = uploadImage(image, adRef).toString();
            dbImage.setValue(imageRef);
        } else {
            dbImage.setValue("gs://enterprise-project-52aff.appspot.com/adImages/No-Image.jpg");
        }

    }

}
