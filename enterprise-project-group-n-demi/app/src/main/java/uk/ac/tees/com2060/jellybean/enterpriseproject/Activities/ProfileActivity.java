package uk.ac.tees.com2060.jellybean.enterpriseproject.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

public class ProfileActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private int accountType;
    private FirebaseStorage storage;
    private TextView mFirstname, Lastname;
    private ImageView mProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usage);

        mAuth = FirebaseAuth.getInstance();

        mFirstname = findViewById(R.id.firstName);
        Lastname = findViewById(R.id.lastName);

        FirebaseUser user = mAuth.getCurrentUser();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users/" + user.getUid());

        ValueEventListener vel = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                accountType = (Integer) dataSnapshot.child("type").getValue();
                mFirstname.setText(dataSnapshot.child("firstName").getValue().toString());
                Lastname.setText(dataSnapshot.child("lastName").getValue().toString());
                switch(accountType) {
                    case 0:
                        //show Object Profile
                        //contact number
                        //favourite list of addresses for collection
                        //feedback from Positioners
                        break;
                    case 1:
                        //show Both Profiles
                        break;
                    case 2:
                        //show Positioner Profile
                        //car model
                        //reg number
                        //contact name
                        //contact number
                        //feedback from Objects
                        break;
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                // ...
            }
        };
    }
}