package uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import uk.ac.tees.com2060.jellybean.enterpriseproject.Objects.Advert;
import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

/**
 * Created by q5318860 on 06/03/18.
 */

public class HomeFragment extends Fragment {

    private final String TAG = "HomeFragment: ";

    static final int PICK_IMAGE_REQUEST = 1;
    private static final int RESULT_OK = -1;
    private ImageView profileImage;
    private View homeFragment;
    private ViewGroup homeFragmentVG;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        homeFragment = inflater.inflate(R.layout.home_fragment, container, false);

        updateProfileInfo(homeFragment);

        homeFragmentVG = (ViewGroup) homeFragment.findViewById(R.id.advertLinearLayout);


        getUserAdverts();


//        Advert a = new Advert("testTitle", "location1", "location2", 98);
//        Advert b = new Advert("testTitle2", "location3", "location4", 43);
//
//        vg.addView(createAdvertOverview(a));
//        vg.addView(createAdvertOverview(b));
//        vg.addView(createAdvertOverview(a));
//        vg.addView(createAdvertOverview(b));
//        vg.addView(createAdvertOverview(a));
//        vg.addView(createAdvertOverview(b));

        return homeFragment;
    }

    public void updateProfileInfo(View v) {

        final TextView firstName = (TextView) v.findViewById(R.id.firstName);
        final TextView lastName = (TextView) v.findViewById(R.id.lastName);
        final TextView email = (TextView) v.findViewById(R.id.email);
        profileImage = v.findViewById(R.id.profileImage);


        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        email.setText(user.getEmail());
        profileImage.setImageURI(user.getPhotoUrl());

        Log.d(TAG, user.getPhotoUrl().toString());

        Uri image = user.getPhotoUrl();
        Glide.with(this)
                .load(image)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(100, 100) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        // Do something with bitmap here.
                        profileImage.setImageBitmap(bitmap);
                    }
                });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeProfileImage();
            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userRef = database.getReference("users/"+user.getUid());

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                firstName.setText(dataSnapshot.child("firstName").getValue().toString());
                lastName.setText(dataSnapshot.child("lastName").getValue().toString());
            }

            @Override public void onCancelled(DatabaseError databaseError) {}

        });
    }

    public void getUserAdverts() {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference gsReference = storage.getReferenceFromUrl("gs://enterprise-project-52aff.appspot.com");

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child("adverts");


        ref.orderByChild("userId").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String adId;
                String userId;
                String title;
                String description;
                String location;
                String destination;
                String collectionDate;
                String deliveryDate;
                 String imageUrl;
                try {
                    adId = dataSnapshot.getKey();
                    userId = dataSnapshot.child("userId").getValue().toString();
                    title = dataSnapshot.child("title").getValue().toString();
                    description = dataSnapshot.child("description").getValue().toString();
                    location = dataSnapshot.child("location").getValue().toString();
                    destination = dataSnapshot.child("destination").getValue().toString();
                    collectionDate = dataSnapshot.child("collectionDate").getValue().toString();
                    deliveryDate = dataSnapshot.child("deliveryDate").getValue().toString();
                    imageUrl = dataSnapshot.child("image").getValue().toString();


                    Advert a = new Advert(adId, userId, title, description, location, destination, collectionDate, deliveryDate, imageUrl);
                    View ad = createAdvertOverview(a);
                    homeFragmentVG.addView(ad);
                } catch (Exception e) {
                    Log.d(TAG, "Failed to add child to Adverts Fragment");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        ref.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // Get Post object and use the values to update the UI
//                Log.d("AAAAA", "FOUND ADVERT: " + dataSnapshot.getValue().toString());
//                if(dataSnapshot.child("userId").getValue().toString().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
//
//                    Log.d("AAAAA", "FOUND ADVERT: " + dataSnapshot.child("userId").getKey());
//                    String title = dataSnapshot.child("Title").toString();
//                    Advert a = new Advert(title, "temp", "temp", 99);
//                    View ad = createAdvertOverview(a);
//                    ViewGroup vg = (ViewGroup) homeFragment;
//                    vg.addView(ad);
//                }
//            }
//
//            @Override public void onCancelled(DatabaseError databaseError) {}
//
//        });

    }

    public View createAdvertOverview(Advert a) {

        homeFragment.findViewById(R.id.advertsLayout).setVisibility(View.VISIBLE);

        View ad = getLayoutInflater().inflate(R.layout.advert_overview, null, false);

        ImageView image = (ImageView) ad.findViewById(R.id.advertImage);
        TextView title = (TextView)ad.findViewById(R.id.advertTitle);
        TextView location = (TextView)ad.findViewById(R.id.locationText1);
        TextView destination = (TextView)ad.findViewById(R.id.locationText2);
        //TextView currentBid = (TextView)ad.findViewById(R.id.currentBid);

        title.setText(a.getTitle());
        location.setText(a.getLocation());
        destination.setText(a.getDestination());
        //currentBid.setText("£"+a.getCurrentBid());

        StorageReference gsReference = FirebaseStorage.getInstance().getReferenceFromUrl("gs://enterprise-project-52aff.appspot.com");
        StorageReference imageRef = gsReference.child("adImages/"+a.getAdId()+".jpg");

        //TODO: Get image from database, this does not work, it worked with Activities, but not with fragments.
        Glide.with(this)
                .using(new FirebaseImageLoader())
                .load(imageRef)
                .into(image);

        return ad;
    }

    private void changeProfileImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri image = data.getData();
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            //TODO: setPhotoUri does not work so will have to set up file storage instead
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(user.getDisplayName())
                    .setPhotoUri(image)
                    .build();

            user.updateProfile(profileUpdates);
            profileImage.setImageURI(image);

        }
    }
}
