package uk.ac.tees.com2060.jellybean.enterpriseproject.Objects;

public class Advert {

    private String adId;
    private String userId;
    private String title;
    private String description;
    private String location;
    private String destination;
    private String collectionDate;
    private String deliveryDate;
    private String imageUrl;


    public String getAdId() {
        return adId;
    }

    public String getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public String getDestination() {
        return destination;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Advert(String adID, String userId, String title, String description, String location, String destination, String collectionDate, String deliveryDate, String imageUrl) {
        this.adId = adId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.location = location;
        this.destination = destination;
        this.collectionDate = collectionDate;
        this.deliveryDate = deliveryDate;
        this.imageUrl = imageUrl;
    }

}
