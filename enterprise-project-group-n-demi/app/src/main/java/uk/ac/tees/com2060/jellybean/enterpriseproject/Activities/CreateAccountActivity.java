package uk.ac.tees.com2060.jellybean.enterpriseproject.Activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

public class CreateAccountActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "CREATE_ACCOUNT";
    private FirebaseAuth mAuth;
    private EditText mEmail, mFirstName, mLastName, mPass, mCPass;
    private CheckBox mShowPassword;
    private Button mSignUp;
    private TextView mSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        mAuth = FirebaseAuth.getInstance();

        mEmail = (EditText) findViewById(R.id.email);
        mFirstName = (EditText) findViewById(R.id.firstName);
        mLastName = (EditText) findViewById(R.id.lastName);
        mPass = (EditText) findViewById(R.id.password);
        mCPass = (EditText) findViewById(R.id.confirmPassword);
        mShowPassword = (CheckBox) findViewById(R.id.showPassword);
        mSignUp = (Button) findViewById(R.id.signUp);
        mSignIn = (TextView) findViewById(R.id.signIn);

        mShowPassword.setOnClickListener(this);
        mSignUp.setOnClickListener(this);
        mSignIn.setOnClickListener(this);
    }

    public void createUser(String email, String password, final String firstName, final String lastName) {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = mAuth.getCurrentUser();
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference myRef = database.getReference("users/" + user.getUid());
                                myRef.child("firstName").setValue(firstName);
                                myRef.child("lastName").setValue(lastName);

                                Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                                        "://" + getResources().getResourcePackageName(R.drawable.ic_menu_camera)
                                        + '/' + getResources().getResourceTypeName(R.drawable.ic_menu_camera) + '/' + getResources().getResourceEntryName(R.drawable.ic_menu_camera) );

                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(firstName + " " + lastName)
                                        .setPhotoUri(imageUri)
                                        .build();

                                user.updateProfile(profileUpdates)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Intent i = new Intent( CreateAccountActivity.this, MainActivity.class);
                                                    startActivity(i);
                                                    finish();
                                                }
                                            }
                                        });
                            }
                        }
                    });
    }

    private boolean validateForm() {
        boolean valid = true;
        if(!validateField(mEmail)) valid = false;
        if(!validateField(mFirstName)) valid = false;
        if(!validateField(mLastName)) valid = false;
        if(!validateField(mPass)) valid = false;
        if(!validateField(mCPass)) valid = false;
        if(!mPass.getText().toString().equals(mCPass.getText().toString())) valid = false;
        return valid;
    }

    public boolean validateField(View view) {
        EditText v = (EditText) view;
        String s = v.getText().toString();
        if (TextUtils.isEmpty(s)) {
            v.setError("Required.");
            return false;
        } else {
            v.setError(null);
            return true;
        }
    }

    @Override
    public void onClick(View click) {
        switch(click.getId()) {
            case R.id.signIn:
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.signUp:
                if (validateForm()) {
                    String email = mEmail.getText().toString();
                    String pass = mPass.getText().toString();
                    String fn = mFirstName.getText().toString();
                    String ln = mLastName.getText().toString();
                    createUser(email, pass, fn, ln);
                }
                break;
            case R.id.showPassword:
                if(mShowPassword.isChecked()) {
                    mPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    mCPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
                else {
                    mPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mCPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                break;
        }
    }
}
