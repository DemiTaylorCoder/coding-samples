package uk.ac.tees.com2060.jellybean.enterpriseproject.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    // Variable declaration
    private FirebaseAuth mAuth;
    private EditText mEmail, mPass;
    private Button mSignIn;
    private TextView mSignUp;
    private CheckBox mShowPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        mEmail = (EditText) findViewById(R.id.email);
        mPass = (EditText) findViewById(R.id.password);
        mShowPassword = (CheckBox) findViewById(R.id.showPassword);
        mSignIn = (Button) findViewById(R.id.signIn);
        mSignUp = (TextView) findViewById(R.id.signUp);

        mShowPassword.setOnClickListener(this);
        mSignIn.setOnClickListener(this);
        mSignUp.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        // If a user is already logged in, redirect to the MainActivity.
        if(mAuth.getCurrentUser() != null) launchMainActivity();
    }

    public void authenticate(String email, String password) {
        if(!validateForm()) return;

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    launchMainActivity();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Authentification Failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void launchMainActivity() {
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private boolean validateForm() {

        boolean valid = true;

        String email = mEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmail.setError("Required.");
            valid = false;
        } else mEmail.setError(null);

        String password = mPass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPass.setError("Required.");
            valid = false;
        } else mPass.setError(null);

        return valid;
    }

    @Override
    public void onClick(View click) {
        switch(click.getId()) {
            case R.id.signIn:
                authenticate(mEmail.getText().toString(), mPass.getText().toString());
                break;
            case R.id.signUp:
                Intent i = new Intent(this, CreateAccountActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.showPassword:
                if(mShowPassword.isChecked()) mPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                else mPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
        }
    }
}
