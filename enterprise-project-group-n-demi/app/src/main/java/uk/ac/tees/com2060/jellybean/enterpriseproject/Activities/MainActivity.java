package uk.ac.tees.com2060.jellybean.enterpriseproject.Activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;

import uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments.CreateAdvertFragment;
import uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments.HomeFragment;
import uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments.MessagesFragment;
import uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments.ProfileFragment;
import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private final String TAG = "MAIN2ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        checkAuth();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragManager = getSupportFragmentManager();
        fragManager.beginTransaction().replace(R.id.flContent, new HomeFragment()).commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        checkAuth();
    }

    public void checkAuth() {
        if(mAuth.getCurrentUser() == null) {
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        if(item.getItemId() == R.id.nav_sign_out) {
            mAuth.signOut();
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);
            return true;
        }

        Fragment frag = null;
        Class fragClass;

        switch(item.getItemId()) {
            case R.id.nav_home:
                fragClass = HomeFragment.class;
                break;
            case R.id.nav_create_advert:
                fragClass = CreateAdvertFragment.class;
                break;
            case R.id.nav_messages:
                fragClass =  MessagesFragment.class;
                break;
            case R.id.nav_profile:
                fragClass =  ProfileFragment.class;
                break;
            case R.id.nav_sign_out:
                mAuth.signOut();
                Intent redirectToLogin = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(redirectToLogin);
                return true;
            default:
                Log.d(TAG, "switch statement default triggered unexpectedly. MenuItem clicked ID: " + item.getItemId());
                fragClass = HomeFragment.class;
        }

        try {
            //frag = (Fragment) fragClass.newInstance();
            frag = (Fragment) fragClass.newInstance();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragManager = getSupportFragmentManager();
        fragManager.beginTransaction().replace(R.id.flContent, frag).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        setTitle(item.getTitle());
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
