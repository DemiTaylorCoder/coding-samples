package uk.ac.tees.com2060.jellybean.enterpriseproject.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.Calendar;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;


public class CreateAdvertActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText mTitle, mDescription;
    private Button mClear, mCancel, mSave;
    private TextView mDate1, mDate2;
    private DatePickerDialog.OnDateSetListener mDate1SetListener, mDate2SetListener;
    private ImageView mImage;
    static final int PICK_IMAGE_REQUEST = 1;
    private Uri filepath;
    private StorageReference mStorageRef;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_advert);

        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mTitle =(EditText) findViewById(R.id.advertTitle);
        mDescription = (EditText) findViewById(R.id.description);

        mClear = (Button) findViewById(R.id.clear);
        mCancel = (Button) findViewById(R.id.cancel);
        mSave = (Button) findViewById(R.id.save);

        mDate1 = (TextView) findViewById(R.id.date1);
        mDate2 = (TextView) findViewById(R.id.date2);

        mImage = (ImageView) findViewById(R.id.image);

        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });

        mDate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        CreateAdvertActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDate1SetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDate1SetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = "";
                if(day<10) date += "0"+day+"/"; else date += day+"/";
                if(month<10) date += "0"+month+"/"+year; else date += month+"/"+year;
                mDate1.setText(date);
            }
        };

        mDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        CreateAdvertActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDate2SetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDate2SetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = "";
                if(day<10) date += "0"+day+"/"; else date += day+"/";
                if(month<10) date += "0"+month+"/"+year; else date += month+"/"+year;
                mDate2.setText(date);
            }
        };

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = mAuth.getCurrentUser().getUid();
                String title = mTitle.getText().toString();
                String desc = mDescription.getText().toString();
                String date1 = mDate1.getText().toString();
                String date2 = mDate2.getText().toString();

                writeDatabase(userId, title, desc, date1, date2, filepath);
            }
        });

        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTitle.setText("");
                mDescription.setText("");
                mDate1.setText("Select Date");
                mDate2.setText("Select Date");

            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateAdvertActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filepath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                mImage.setImageBitmap(bitmap);


            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public StorageReference uploadImage(Uri image, final String adId) {
        StorageReference imageRef = mStorageRef.child("adImages/"+adId+".jpg");

        imageRef.putFile(image).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                launchDisplayAdvertActivity(adId);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
        return imageRef;
    }

    private void launchDisplayAdvertActivity(String adRef) {
        Intent i = new Intent(getApplicationContext(), DisplayAdvertActivity.class);
        if(adRef != null) i.putExtra("adId", adRef);
        startActivity(i);
    }

    public void writeDatabase(String userId, String title, String desc, String date1, String date2, Uri image) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference dbRoot = db.getReference("adverts");
        String adRef = dbRoot.push().getKey().toString();
        DatabaseReference dbUserId = db.getReference("adverts/"+adRef+"/userId");
        DatabaseReference dbTitle = db.getReference("adverts/"+adRef+"/Title");
        DatabaseReference dbDesc = db.getReference("adverts/"+adRef+"/Description");
        DatabaseReference dbDate1 = db.getReference("adverts/"+adRef+"/Date 1");
        DatabaseReference dbDate2 = db.getReference("adverts/"+adRef+"/Date 2");
        DatabaseReference dbImage = db.getReference("adverts/"+adRef+"/Image");

//        DatabaseReference temp = db.getReference("adverts/"+adRef+"/test");
//        temp.setValue("test");
        dbUserId.setValue(userId);
        dbTitle.setValue(title);
        dbDesc.setValue(desc);
        dbDate1.setValue(date1);
        dbDate2.setValue(date2);
        if(image != null) {
            String imageRef = uploadImage(image, adRef).toString();
            dbImage.setValue(imageRef);
        } else {
            dbImage.setValue("gs://enterprise-project-52aff.appspot.com/adImages/No-Image.jpg");
            launchDisplayAdvertActivity(null);
        }

    }


}
