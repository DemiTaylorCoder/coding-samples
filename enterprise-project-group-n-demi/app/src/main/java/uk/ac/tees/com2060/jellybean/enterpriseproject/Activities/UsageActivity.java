package uk.ac.tees.com2060.jellybean.enterpriseproject.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

public class UsageActivity extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private TextView mTitleObject, mTextObject, mTitleBoth, mTextBoth, mTitlePositioner, mTextPositioner;
    private SeekBar mUsage;
    private FirebaseAuth mAuth;
    private Button mConfirm, mCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usage);

        mAuth = FirebaseAuth.getInstance();

        mUsage = (SeekBar) findViewById(R.id.Usage);
        mTitleObject = (TextView) findViewById(R.id.titleObject);
        mTextObject = (TextView) findViewById(R.id.textObject);
        mTitleBoth = (TextView) findViewById(R.id.titleBoth);
        mTextBoth = (TextView) findViewById(R.id.textBoth);
        mTitlePositioner = (TextView) findViewById(R.id.titlePositioner);
        mTextPositioner = (TextView) findViewById(R.id.textPositioner);
        mConfirm = (Button) findViewById(R.id.confirm);
        mCancel = (Button) findViewById(R.id.cancel);

        mConfirm.setOnClickListener(this);
        mCancel.setOnClickListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
        switch(progressValue) {
            case 0:
                mTitleBoth.setVisibility(View.INVISIBLE);
                mTextBoth.setVisibility(View.INVISIBLE);
                mTitlePositioner.setVisibility(View.INVISIBLE);
                mTextPositioner.setVisibility(View.INVISIBLE);
                mTitleObject.setVisibility(View.VISIBLE);
                mTextObject.setVisibility(View.VISIBLE);
                break;
            case 1:
                mTitleObject.setVisibility(View.INVISIBLE);
                mTextObject.setVisibility(View.INVISIBLE);
                mTitlePositioner.setVisibility(View.INVISIBLE);
                mTextPositioner.setVisibility(View.INVISIBLE);
                mTitleBoth.setVisibility(View.VISIBLE);
                mTextBoth.setVisibility(View.VISIBLE);
                break;
            case 2:
                mTitleObject.setVisibility(View.INVISIBLE);
                mTextObject.setVisibility(View.INVISIBLE);
                mTitleBoth.setVisibility(View.INVISIBLE);
                mTextBoth.setVisibility(View.INVISIBLE);
                mTitlePositioner.setVisibility(View.VISIBLE);
                mTextPositioner.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar mUsage) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar mUsage) {
        int seekBarValue = mUsage.getProgress();
        onProgressChanged(mUsage, seekBarValue, true);
    }

    @Override
    public void onClick(View click) {
        switch(click.getId()) {
            case R.id.confirm:
                FirebaseUser user = mAuth.getCurrentUser();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("users/" + user.getUid());

                int seekBarValue = mUsage.getProgress();
                switch(seekBarValue) {
                    case 0:
                        myRef.child("type").setValue("0");
                        break;
                    case 1:
                        myRef.child("type").setValue("1");
                        break;
                    case 2:
                        myRef.child("type").setValue("2");
                        break;
                }

                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.cancel:
                Intent j = new Intent(this, LoginActivity.class);
                startActivity(j);
                finish();
                break;
        }

    }
}
