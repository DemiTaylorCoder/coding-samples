package uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

/**
 * Created by q5318860 on 06/03/18.
 */

public class ProfileFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View profileFragment =  inflater.inflate(R.layout.activity_profile, container, false);

        return profileFragment;
    }


}
