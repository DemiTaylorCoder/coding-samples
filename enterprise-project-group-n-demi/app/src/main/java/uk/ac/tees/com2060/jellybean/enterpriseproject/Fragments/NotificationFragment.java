package uk.ac.tees.com2060.jellybean.enterpriseproject.Fragments;

import android.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import uk.ac.tees.com2060.jellybean.enterpriseproject.R;

/**
 * Created by s6107137 on 13/03/18.
 */

public class NotificationFragment extends FirebaseMessagingService {

    private CharSequence textTitle;
    private CharSequence textContent;

    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, null)
            .setSmallIcon(android.support.v4.R.drawable.notification_icon_background)
            .setContentTitle(textTitle)
            .setContentText(textContent)
            .setStyle(new NotificationCompat.BigTextStyle()
                    .bigText("Much longer text that cannot fit one line..."))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

    private static final String TAG = "FCM Service";


    public void onMessageRecived(RemoteMessage remoteMessage){

        String FromUser = remoteMessage.getMessageType();
        String Title = remoteMessage.getNotification().getTitle();
        String Message = remoteMessage.getNotification().getBody();



        Log.d(TAG, FromUser);
        Log.d(TAG, Title);
        Log.d(TAG, Message);
     //Log.d(TAG,"From" + remoteMessageeMessage.getFrom());
     //Log.d(TAG, "Notification Message Body" + remoteMessage.getNotification().getBody());


    }


}
