export class Capsules {
  id: number | undefined;
  landings: number | undefined;
  mission: string | undefined;
  original_launch: string | undefined;
  reuse_count: number | undefined;
  status: string | undefined;
  type: string | undefined;
}