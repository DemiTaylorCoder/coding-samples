import { Component, Input  } from '@angular/core';


@Component({
  selector: 'app-capsule',
  templateUrl: './capsule.component.html',
  styleUrls: ['./capsule.component.sass']
 
})

export class CapsuleComponent {
  @Input() capsule: any;
}


