import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Capsules} from '../Model/capsules.model';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiUrl = 'https://api.spacex.land/rest/capsules';

  constructor(private _http: HttpClient) {}

  getCapsules(){
    return this._http.get<Capsules[]>(this.apiUrl);
  }
}
