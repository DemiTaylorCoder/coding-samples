import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { DataService} from './Services/data.service';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { CapsuleComponent } from './components/capsule/capsule.component';


@NgModule({
  declarations: [
    AppComponent,
    FormatDatePipe,
    CapsuleComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
