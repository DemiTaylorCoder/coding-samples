import { Component, OnInit } from '@angular/core';
import {Capsules} from './Model/capsules.model';
import {DataService} from './Services/data.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  
})
export class AppComponent implements OnInit {
  capsules!: Capsules[];

  constructor(private dataService: DataService) {}

  ngOnInit(){
    return this.dataService.getCapsules()
      .subscribe(data => this.capsules = data)
  }

}

