﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRTwoHandGrab : XRGrabInteractable
{
    public List<XRSimpleInteractable> secondHandGrabPoints = new List<XRSimpleInteractable>();
    private XRBaseInteractor secondInteractor;
    private Quaternion attaachInitialRotation, initialRotationOffset;
    public enum TwoHandRotationType {None, First, Second };
    public TwoHandRotationType twoHandRotationType;
    public bool snapToSecondHand = false;

    
    // Start is called before the first frame update
    public void Start()
    {
        foreach(var item in secondHandGrabPoints)
        {
            item.onSelectEnter.AddListener(OnSecondHandGrab);
            item.onSelectEnter.AddListener(OnSecondHandRelease);
        }
        
    }

    // Update is called once per frame
   public void Update()
    {
       
    }
    // Switching between different interactables.
    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        base.ProcessInteractable(updatePhase);
        if (secondInteractor && selectingInteractor)
        {
            if(snapToSecondHand)
                selectingInteractor.attachTransform.rotation = GetTwoHandRotation();
            else
                selectingInteractor.attachTransform.rotation = GetTwoHandRotation() * initialRotationOffset;

        }
        
    }
    // allows the user to rotate the game object
    private Quaternion GetTwoHandRotation()
    {
        Quaternion targetRotation;
        if (twoHandRotationType == TwoHandRotationType.None)
        {
            targetRotation = Quaternion.LookRotation(secondInteractor.attachTransform.position - selectingInteractor.attachTransform.position);
        }
        else if(twoHandRotationType == TwoHandRotationType.First)
        {
            targetRotation = Quaternion.LookRotation(secondInteractor.attachTransform.position - selectingInteractor.attachTransform.position, selectingInteractor.attachTransform.up);
        }
        else
        {
            targetRotation = Quaternion.LookRotation(secondInteractor.attachTransform.position - selectingInteractor.attachTransform.position, secondInteractor.attachTransform.up);
        }
        return targetRotation;
    }
    // allows the user to grab with the second hand
    public void OnSecondHandGrab(XRBaseInteractor interactor)
    {
        Debug.Log("Second Hand Connected");
        secondInteractor = interactor;
        initialRotationOffset = Quaternion.Inverse(GetTwoHandRotation()) * selectingInteractor.attachTransform.rotation;
    }
    // allows the user to release the object
    public void OnSecondHandRelease(XRBaseInteractor interactor)
    {
        Debug.Log("Second Hand Disconnected");
        secondInteractor = null;
    }
   // allows the first hand to grab
    protected override void OnSelectEnter(XRBaseInteractor interactor)
    {
        Debug.Log("Fist Hand Connected");
        base.OnSelectEnter(interactor);
        attaachInitialRotation = interactor.attachTransform.localRotation;
    }
    // release when first hand is released
    protected override void OnSelectExit(XRBaseInteractor interactor)
    {
        Debug.Log("First Hand Disconnected");
        base.OnSelectExit(interactor);
        secondInteractor = null;
        interactor.attachTransform.localRotation = attaachInitialRotation;
    }
    // when first hand is attached allow the second hand to grab.
    public override bool IsSelectableBy(XRBaseInteractor interactor)
    {
        bool isalreadygrabbed = selectingInteractor && !interactor.Equals(selectingInteractor);
        return base.IsSelectableBy(interactor) && !isalreadygrabbed;
    }
}
