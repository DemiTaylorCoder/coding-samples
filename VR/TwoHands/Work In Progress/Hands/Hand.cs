using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class Hand : XRDirectInteractor
{
    //creates the skin mesh
    private SkinnedMeshRenderer meshRenderer = null;
    
    protected override void Awake()
    {
        base.Awake();
        meshRenderer = GetComponentInChildren<SkinnedMeshRenderer>(); 
        
    }
   //hides the hands
    public void SetVisibility(bool value)
    {
        meshRenderer.enabled = value;
    }
}
