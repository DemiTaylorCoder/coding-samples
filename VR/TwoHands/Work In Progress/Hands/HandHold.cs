using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class HandHold : XRBaseInteractable
{
    //this get the game object which in the players hands
    public GameObject objectInHand;
    //weaponpart verible to be instansiated
    protected WeaponParts WeaponPart = null;

    //this instantiates the weaponpart.
    public void Setup(WeaponParts weaponPart)
    {
        this.WeaponPart = weaponPart;
    }
    // this adds the listeners to the object
    protected override void Awake()
    {
        base.Awake();
        onSelectEntered.AddListener(Grab);
        onSelectExited.AddListener(Drop);
    }
    //this removes the listeners from the object
    protected override void OnDestroy()
    {
        onSelectEntered.RemoveListener(Grab);
        onSelectExited.RemoveListener(Drop);
    }

    protected virtual void BeginAction(XRBaseInteractor interactor)
    {
        // Empty
    }

    protected virtual void EndAction(XRBaseInteractor interactor)
    {
        // Empty
    }
    //this allows the player to grab the object
    protected virtual void Grab(XRBaseInteractor interactor)
    {
        TryToHideHand(interactor, true);
        var joint = AddFixedJoint();
        joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
    }
    // allows the player to drop the object
    protected virtual void Drop(XRBaseInteractor interactor)
    {
        TryToHideHand(interactor, false);
        
    }
    // this creates a joint between the hand and the object
    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }
    //Trys to hide the hands
    public void TryToHideHand(XRBaseInteractor interactor, bool hide)
    {
        if(interactor is Hand hand)
        {
            hand.SetVisibility(hide);
        }
    }

    public void BreakHold(XRBaseInteractor interactor)
    {
        Drop(interactor);
    }
}
