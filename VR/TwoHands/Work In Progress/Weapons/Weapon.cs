using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    //Sets The two gameObjects
    public WeaponParts weapon1;
    public WeaponParts weapon2;

    public Vector3 positionOffset;
    public Quaternion rotationOffset;
    public bool snapToBothHands;

    //this is deals with rotation and which hand should do it
    public enum TwoHandRotationType { none, first, second};
    public TwoHandRotationType twohandrotationtype;
  

    private void FixedUpdate()
    {
        //This checks to see if weapon part one is grabbed
        if(weapon1.isAreadyGrabed == true)
        {
            Debug.Log("Weapon 1 one is Grabbed");
            //This checks to see if weapon part two is grabbed
            if (weapon2.isAreadyGrabed == true)
            {
                Debug.Log("Weapon 2 one is Grabbed");
            }
        }
        //this checks if both weapons have been gripped
        if (weapon1.gripHand && weapon2.gripHand)
        {
            if (snapToBothHands)
            {
                Vector3 vector = new Vector3(0, 0, 0);
                weapon1.gripHold.transform.RotateAround(weapon1.gripHold.objectInHand.transform.position, vector, 90f);
                weapon1.gripHand.attachTransform.rotation = BothHandsPositionsandRotation();
                
            }
            else
            {
                weapon1.attachTransform.rotation = BothHandsPositionsandRotation() * rotationOffset;
                
            }
            
        }

        BothHandsPositionsandRotation();
    }
// this set the rotation of the weapon
    public Quaternion BothHandsPositionsandRotation()
    {

        Quaternion targetRotation;
        if (twohandrotationtype == TwoHandRotationType.none)
        {
            targetRotation = Quaternion.LookRotation(weapon2.attachTransform.position - weapon1.attachTransform.position);
        }
        else if (twohandrotationtype == TwoHandRotationType.first)
        {
            targetRotation = Quaternion.LookRotation(weapon2.attachTransform.position - weapon1.attachTransform.position, weapon1.attachTransform.up);
            
        }
        else
        {
            targetRotation = Quaternion.LookRotation(weapon2.attachTransform.position - weapon1.attachTransform.position, weapon2.attachTransform.up);
        }
       
        return targetRotation;

    }

}
