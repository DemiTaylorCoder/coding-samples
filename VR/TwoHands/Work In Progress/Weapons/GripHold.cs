using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class GripHold : HandHold
{
    

    protected override void BeginAction(XRBaseInteractor interactor)
    {
        base.BeginAction(interactor);
    }

    protected override void EndAction(XRBaseInteractor interactor)
    {
        base.BeginAction(interactor);
    }
    // this overrides the HandHold Grab fuction
    protected override void Grab(XRBaseInteractor interactor)
    {
        base.Grab(interactor);
        Debug.Log("Grabbing Object at main Point");
        WeaponPart.SetGripHand(interactor);
        TryToHideHand(interactor, true);
    }
    //this overrides the HandHold Drop Function
    protected override void Drop(XRBaseInteractor interactor)
    {
        base.Drop(interactor);
        WeaponPart.ClearGripHand(interactor);
        TryToHideHand(interactor, false);

    }
}
