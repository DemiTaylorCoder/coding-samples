using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
public class WeaponParts : XRGrabInteractable
{
    //this sets the break distance;
    public float breakDistance = 0.25f;
    //this set the grip for the hand
    public GripHold gripHold = null;
    //this set the interactor for the hand
    public XRBaseInteractor gripHand = null;
    //this checks if the player has grabbed the object;
    public bool isAreadyGrabed = false;
    protected override void Awake()
    {
        base.Awake();
        SetupHolds();
    }
    //this create's the holdes
    private void SetupHolds()
    {
        gripHold = GetComponentInChildren<GripHold>();
        gripHold.Setup(this);

    }

    protected override void OnDestroy()
    {
        // to be exted in future
    }

    //this set the grip hand
    public void SetGripHand(XRBaseInteractor interactor)
    {
        isAreadyGrabed = true;
        Debug.Log("Grabbing Object a with Main Point");
        gripHand = interactor;
        OnSelectEntered(gripHand);
    }
    //allows the player to drop the object
    public void ClearGripHand(XRBaseInteractor interactor)
    {
        isAreadyGrabed = false;
        Debug.Log("Dropping Object at Main Point");
        gripHand = interactor;
        OnSelectExited(gripHand);
    }
    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        base.ProcessInteractable(updatePhase);
        CheckDistance(gripHand, gripHold);
    }
    //this checks the distance between the hand and object and breaks the connection
    private void CheckDistance(XRBaseInteractor interactor, HandHold handHold)
    {
        if (interactor)
        {
            float distanceSqr = handHold.GetDistanceSqrToInteractor(interactor);

            if (distanceSqr > breakDistance)
                handHold.BreakHold(interactor);
        }
    }
}
