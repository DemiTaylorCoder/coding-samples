﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.EntityFrameworkCore;


namespace Data.Staff
{
    public class Staff
    {
        public int StaffId { get; set; }
        public string Name { get; set; }
        public Boolean IsManager { get; set; }
        public int PermissionId { get; set; }
        public Permissions Permission { get; set; }
        public int RoleId { get; set; }
        public Roles Role { get; set; }

        public bool IsActive { get; set; }

  
    }
}
