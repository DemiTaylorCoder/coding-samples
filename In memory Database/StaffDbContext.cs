﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Data.Staff
{
    public class StaffDbContext : DbContext
    {
        public  DbSet<Staff> Staffs { get; set; }
        public  DbSet<Roles> Role { get; set; }
        public  DbSet<Permissions> Permission { get; set; }

        public StaffDbContext(){

        }
        public StaffDbContext(DbContextOptions<StaffDbContext> options) : base(options)
        {
            
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<Staff>(staffEntity =>
            {
                staffEntity.HasKey(staff => staff.StaffId);
                staffEntity.Property(staff => staff.StaffId).ValueGeneratedOnAdd(); ;

                staffEntity.Property(staff => staff.Name).IsRequired();

                staffEntity.HasOne(staff => staff.Permission)
                           .WithMany()
                           .HasForeignKey(staff => staff.PermissionId);

                staffEntity.HasOne(staff => staff.Role)
                           .WithMany()
                           .HasForeignKey(staff => staff.RoleId);

                staffEntity.Property(staff => staff.IsManager).IsRequired();
                staffEntity.Property(staff => staff.IsActive).IsRequired();
            });

            modelBuilder.Entity<Roles>(roleEntity =>
            {
                roleEntity.HasKey(role => role.RoleId);
                roleEntity.Property(role => role.RoleId).ValueGeneratedOnAdd();
                roleEntity.Property(role => role.RoleName).IsRequired();
                roleEntity.Property(role => role.IsActive).IsRequired();


            });
            
            modelBuilder.Entity<Permissions>(permissionEntity =>
            {
                permissionEntity.HasKey(permission => permission.PermissionsId);
                permissionEntity.Property(permission => permission.PermissionsId).ValueGeneratedOnAdd();
                permissionEntity.Property(permission => permission.LevelName).IsRequired();
                permissionEntity.Property(permission => permission.EditMembers).IsRequired();
                permissionEntity.Property(permission => permission.EditStaff).IsRequired();
                permissionEntity.Property(permission => permission.MakeOrders).IsRequired();
                permissionEntity.Property(permission => permission.EditOrders).IsRequired();
                permissionEntity.Property(permission => permission.ApproveOrders).IsRequired();
                permissionEntity.Property(permission => permission.CreateStaff).IsRequired();
                permissionEntity.Property(permission => permission.AllowedRestock).IsRequired();
                permissionEntity.Property(permission => permission.AllowedThirdparty).IsRequired();
                permissionEntity.Property(Permission => Permission.IsActive).IsRequired();
            });
         

        }

       
    }
}
