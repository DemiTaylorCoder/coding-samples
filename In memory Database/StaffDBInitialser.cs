﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System.Linq;

namespace Data.Staff
{
    class StaffDBInitialser
    {
        public static async Task SeedTestData(StaffDbContext context, IServiceProvider service)
        {
            if (context.Staffs.Any())
            {
                return;
            }
            if (context.Permission.Any())
            {
                return;
            }
            if (context.Role.Any())
            {
                return;
            }

            List<Staff> staff = new List<Staff>()
            {
                new Staff {StaffId = 1,Name = "Joe", IsActive = true, RoleId = 1, PermissionId = 1,IsManager = false},
                new Staff {StaffId = 1,Name = "Fred", IsActive = true, RoleId = 2, PermissionId = 2,IsManager = false},
                new Staff {StaffId = 1,Name = "Chris", IsActive = false, RoleId = 1, PermissionId = 1,IsManager = false},
                new Staff {StaffId = 2,Name = "Sam", IsActive = true, RoleId = 3, PermissionId = 3,IsManager = true}
            };
            staff.ForEach(s => context.Staffs.Add(s));
            
            List<Roles> role = new List<Roles>
            {
                new Roles {RoleId = 1, RoleName = "Admin"},
                new Roles {RoleId = 2, RoleName = "Staff"},
                new Roles {RoleId = 3, RoleName = "Manager"},
            };
            role.ForEach(r => context.Role.Add(r));

            List<Permissions> permission = new List<Permissions>
            {
                new Permissions {PermissionsId = 1,LevelName = "Admin",EditMembers = true,EditOrders = false,EditStaff = false,AllowedRestock = false,AllowedThirdparty = false,ApproveOrders = false,MakeOrders = false,CreateStaff = false},
                new Permissions {PermissionsId = 2,LevelName = "Staff",EditMembers = true,EditOrders = true,EditStaff = false,AllowedRestock = true,AllowedThirdparty = true,ApproveOrders = false,MakeOrders = true,CreateStaff = false},
                new Permissions {PermissionsId = 3,LevelName = "Manager",EditMembers = true,EditOrders = true,EditStaff = true,AllowedRestock = true,AllowedThirdparty = true,ApproveOrders = true,MakeOrders = true,CreateStaff = true}
            };
            permission.ForEach(p => context.Permission.Add(p));


            await context.SaveChangesAsync();
        }
    }
}
