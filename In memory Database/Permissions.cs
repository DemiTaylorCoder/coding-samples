﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Staff
{
    public class Permissions
    {
        public int PermissionsId { get; set; }
        public string LevelName { get; set; }
        public Boolean EditMembers { get; set; }
        public Boolean EditStaff { get; set; }
        public Boolean MakeOrders { get; set; }
        public Boolean EditOrders { get; set; }
        public Boolean ApproveOrders { get; set; }
        public Boolean CreateStaff { get; set; }
        public Boolean AllowedRestock { get; set; }
        public Boolean AllowedThirdparty { get; set; }

        public bool IsActive { get; set; }

    }
}
