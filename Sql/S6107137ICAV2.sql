USE [master]
GO
/****** Object:  Database [s6107137]    Script Date: 06/11/2018 13:47:49 ******/
CREATE DATABASE [s6107137]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N's6107137', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\s6107137.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N's6107137_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\s6107137_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [s6107137] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [s6107137].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [s6107137] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [s6107137] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [s6107137] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [s6107137] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [s6107137] SET ARITHABORT OFF 
GO
ALTER DATABASE [s6107137] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [s6107137] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [s6107137] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [s6107137] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [s6107137] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [s6107137] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [s6107137] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [s6107137] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [s6107137] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [s6107137] SET  DISABLE_BROKER 
GO
ALTER DATABASE [s6107137] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [s6107137] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [s6107137] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [s6107137] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [s6107137] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [s6107137] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [s6107137] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [s6107137] SET RECOVERY FULL 
GO
ALTER DATABASE [s6107137] SET  MULTI_USER 
GO
ALTER DATABASE [s6107137] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [s6107137] SET DB_CHAINING OFF 
GO
ALTER DATABASE [s6107137] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [s6107137] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [s6107137] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N's6107137', N'ON'
GO
ALTER DATABASE [s6107137] SET QUERY_STORE = OFF
GO
USE [s6107137]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [s6107137]
GO
/****** Object:  Table [dbo].[Advert]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Advert](
	[Ad_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[Image] [image] NULL,
	[Total Paid] [float] NULL,
	[Price per Click] [float] NULL,
	[Active] [tinyint] NULL,
 CONSTRAINT [PK_Advert] PRIMARY KEY CLUSTERED 
(
	[Ad_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FollowTopic]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FollowTopic](
	[User_ID] [int] NULL,
	[Topic_ID] [int] NULL,
	[Active] [tinyint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Forum]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forum](
	[Forum_ID] [int] IDENTITY(1,1) NOT NULL,
	[Forum_Name] [nchar](50) NULL,
	[ForumDecription] [nchar](255) NULL,
	[Active] [tinyint] NULL,
 CONSTRAINT [PK_Forum] PRIMARY KEY CLUSTERED 
(
	[Forum_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[Post_ID] [int] IDENTITY(1,1) NOT NULL,
	[PostText] [nchar](255) NULL,
	[Created_At] [timestamp] NULL,
	[User_ID] [int] NULL,
	[Updated_At] [datetime] NULL,
	[Active] [tinyint] NULL,
 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
(
	[Post_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Role_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](20) NULL,
	[AccessLevel] [int] NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Role_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[State_ID] [int] IDENTITY(1,1) NOT NULL,
	[State_Name] [nchar](20) NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[State_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tags]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tags](
	[Tag_ID] [int] IDENTITY(1,1) NOT NULL,
	[Tag_Name] [nchar](15) NULL,
 CONSTRAINT [PK_Tags] PRIMARY KEY CLUSTERED 
(
	[Tag_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Terms]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Terms](
	[Terms_ID] [int] IDENTITY(1,1) NOT NULL,
	[TermsSubject] [nchar](16) NULL,
	[TermsContent] [nchar](100) NULL,
	[TermsChanged] [nchar](50) NULL,
	[TermsCreateDate] [date] NULL,
	[TermsChangeDate] [date] NULL,
 CONSTRAINT [PK_Terms] PRIMARY KEY CLUSTERED 
(
	[Terms_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TermsAgreed]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TermsAgreed](
	[Terms_ID] [int] NULL,
	[User_ID] [int] NULL,
	[TermsAgreedDate] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Topics]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Topics](
	[Topic_ID] [int] IDENTITY(1,1) NOT NULL,
	[TopicTitle] [nchar](25) NULL,
	[Created_At] [datetime] NULL,
	[Updated_At] [datetime] NULL,
	[Active] [tinyint] NULL,
 CONSTRAINT [PK_Topics] PRIMARY KEY CLUSTERED 
(
	[Topic_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TopicTP]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TopicTP](
	[Topic_ID] [int] NOT NULL,
	[Tag_ID] [int] NOT NULL,
	[Post_ID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[User_ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nchar](50) NULL,
	[Email] [nchar](100) NULL,
	[Password] [nchar](16) NULL,
	[FirstName] [nchar](100) NULL,
	[LastName] [nchar](100) NULL,
	[LastLogin] [timestamp] NULL,
	[DateJoined] [date] NULL,
	[Active] [tinyint] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[User_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAdvert]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAdvert](
	[Ad_ID] [int] NULL,
	[User_ID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserFT]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserFT](
	[User_ID] [int] NOT NULL,
	[Forum_ID] [int] NOT NULL,
	[Topic_ID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[User_ID] [int] NOT NULL,
	[Role_ID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserStates]    Script Date: 06/11/2018 13:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserStates](
	[User_ID] [int] NULL,
	[State_ID] [int] NULL,
	[User2_ID] [int] NULL,
	[Active] [tinyint] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Advert] ON 

INSERT [dbo].[Advert] ([Ad_ID], [Name], [Image], [Total Paid], [Price per Click], [Active]) VALUES (1, N'Amazon                        ', NULL, 1000, 0.05, 1)
INSERT [dbo].[Advert] ([Ad_ID], [Name], [Image], [Total Paid], [Price per Click], [Active]) VALUES (2, N'Google', NULL, 5000, 1, 1)
SET IDENTITY_INSERT [dbo].[Advert] OFF
SET IDENTITY_INSERT [dbo].[Forum] ON 

INSERT [dbo].[Forum] ([Forum_ID], [Forum_Name], [ForumDecription], [Active]) VALUES (1, N'Health                                            ', N'Talk about new health care facility and advancements                                                                                                                                                                                                           ', 1)
SET IDENTITY_INSERT [dbo].[Forum] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([User_ID], [Username], [Email], [Password], [FirstName], [LastName], [DateJoined], [Active]) VALUES (1, N'missdemitn                                        ', N'37261@nowhere.com                                                                                   ', N'123Password     ', N'Demi                                                                                                ', N'nimmo                                                                                               ', CAST(N'2018-11-06' AS Date), 1)
INSERT [dbo].[User] ([User_ID], [Username], [Email], [Password], [FirstName], [LastName], [DateJoined], [Active]) VALUES (2, N'thedarkside                                       ', N'38273@nowhere.com                                                                                   ', N'7362Password    ', N'Rory                                                                                                ', N'Rayns                                                                                               ', CAST(N'2018-11-13' AS Date), 1)
INSERT [dbo].[User] ([User_ID], [Username], [Email], [Password], [FirstName], [LastName], [DateJoined], [Active]) VALUES (3, N'powermaster                                       ', N'7263n272@blobinksan.com                                                                             ', N'ejshagsjd       ', N'Franky                                                                                              ', N'owns                                                                                                ', CAST(N'2018-11-14' AS Date), 1)
INSERT [dbo].[User] ([User_ID], [Username], [Email], [Password], [FirstName], [LastName], [DateJoined], [Active]) VALUES (4, N'santagiving                                       ', N'santagiving@nothing.com                                                                             ', N'santa1giving2   ', N'Sarah                                                                                               ', N'Sonic                                                                                               ', CAST(N'2018-11-19' AS Date), 1)
INSERT [dbo].[User] ([User_ID], [Username], [Email], [Password], [FirstName], [LastName], [DateJoined], [Active]) VALUES (5, N'billysonic                                        ', N'sonicsync@skrmail.net                                                                               ', N'djdhfss23       ', N'nowhere                                                                                             ', N'billy                                                                                               ', CAST(N'2018-12-01' AS Date), 0)
SET IDENTITY_INSERT [dbo].[User] OFF
INSERT [dbo].[UserAdvert] ([Ad_ID], [User_ID]) VALUES (1, 1)
INSERT [dbo].[UserAdvert] ([Ad_ID], [User_ID]) VALUES (2, 1)
ALTER TABLE [dbo].[FollowTopic]  WITH CHECK ADD  CONSTRAINT [FK_FollowTopic_Topics] FOREIGN KEY([Topic_ID])
REFERENCES [dbo].[Topics] ([Topic_ID])
GO
ALTER TABLE [dbo].[FollowTopic] CHECK CONSTRAINT [FK_FollowTopic_Topics]
GO
ALTER TABLE [dbo].[FollowTopic]  WITH CHECK ADD  CONSTRAINT [FK_FollowTopic_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[FollowTopic] CHECK CONSTRAINT [FK_FollowTopic_User]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_User]
GO
ALTER TABLE [dbo].[TermsAgreed]  WITH CHECK ADD  CONSTRAINT [FK_TermsAgreed_Terms] FOREIGN KEY([Terms_ID])
REFERENCES [dbo].[Terms] ([Terms_ID])
GO
ALTER TABLE [dbo].[TermsAgreed] CHECK CONSTRAINT [FK_TermsAgreed_Terms]
GO
ALTER TABLE [dbo].[TermsAgreed]  WITH CHECK ADD  CONSTRAINT [FK_TermsAgreed_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[TermsAgreed] CHECK CONSTRAINT [FK_TermsAgreed_User]
GO
ALTER TABLE [dbo].[TopicTP]  WITH CHECK ADD  CONSTRAINT [FK_TopicTag_Tags] FOREIGN KEY([Tag_ID])
REFERENCES [dbo].[Tags] ([Tag_ID])
GO
ALTER TABLE [dbo].[TopicTP] CHECK CONSTRAINT [FK_TopicTag_Tags]
GO
ALTER TABLE [dbo].[TopicTP]  WITH CHECK ADD  CONSTRAINT [FK_TopicTag_Topics] FOREIGN KEY([Topic_ID])
REFERENCES [dbo].[Topics] ([Topic_ID])
GO
ALTER TABLE [dbo].[TopicTP] CHECK CONSTRAINT [FK_TopicTag_Topics]
GO
ALTER TABLE [dbo].[TopicTP]  WITH CHECK ADD  CONSTRAINT [FK_TopicTP_Post] FOREIGN KEY([Post_ID])
REFERENCES [dbo].[Post] ([Post_ID])
GO
ALTER TABLE [dbo].[TopicTP] CHECK CONSTRAINT [FK_TopicTP_Post]
GO
ALTER TABLE [dbo].[UserAdvert]  WITH CHECK ADD  CONSTRAINT [FK_UserAdvert_Advert] FOREIGN KEY([Ad_ID])
REFERENCES [dbo].[Advert] ([Ad_ID])
GO
ALTER TABLE [dbo].[UserAdvert] CHECK CONSTRAINT [FK_UserAdvert_Advert]
GO
ALTER TABLE [dbo].[UserAdvert]  WITH CHECK ADD  CONSTRAINT [FK_UserAdvert_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[UserAdvert] CHECK CONSTRAINT [FK_UserAdvert_User]
GO
ALTER TABLE [dbo].[UserFT]  WITH CHECK ADD  CONSTRAINT [FK_UserFT_Forum] FOREIGN KEY([Forum_ID])
REFERENCES [dbo].[Forum] ([Forum_ID])
GO
ALTER TABLE [dbo].[UserFT] CHECK CONSTRAINT [FK_UserFT_Forum]
GO
ALTER TABLE [dbo].[UserFT]  WITH CHECK ADD  CONSTRAINT [FK_UserFT_Topics] FOREIGN KEY([Topic_ID])
REFERENCES [dbo].[Topics] ([Topic_ID])
GO
ALTER TABLE [dbo].[UserFT] CHECK CONSTRAINT [FK_UserFT_Topics]
GO
ALTER TABLE [dbo].[UserFT]  WITH CHECK ADD  CONSTRAINT [FK_UserFT_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[UserFT] CHECK CONSTRAINT [FK_UserFT_User]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([Role_ID])
REFERENCES [dbo].[Roles] ([Role_ID])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_User]
GO
ALTER TABLE [dbo].[UserStates]  WITH CHECK ADD  CONSTRAINT [FK_UserStates_State] FOREIGN KEY([State_ID])
REFERENCES [dbo].[State] ([State_ID])
GO
ALTER TABLE [dbo].[UserStates] CHECK CONSTRAINT [FK_UserStates_State]
GO
ALTER TABLE [dbo].[UserStates]  WITH CHECK ADD  CONSTRAINT [FK_UserStates_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[UserStates] CHECK CONSTRAINT [FK_UserStates_User]
GO
ALTER TABLE [dbo].[UserStates]  WITH CHECK ADD  CONSTRAINT [FK_UserStates_User1] FOREIGN KEY([User2_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[UserStates] CHECK CONSTRAINT [FK_UserStates_User1]
GO
USE [master]
GO
ALTER DATABASE [s6107137] SET  READ_WRITE 
GO
